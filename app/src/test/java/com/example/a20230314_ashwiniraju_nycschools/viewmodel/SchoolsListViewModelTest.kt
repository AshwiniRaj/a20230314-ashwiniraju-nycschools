package com.example.a20230314_ashwiniraju_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SchoolListUIData
import com.example.a20230314_ashwiniraju_nycschools.usecase.GetSchoolsListUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * A Unit test class for SchoolsListViewModel.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
internal class SchoolsListViewModelTest {
    private lateinit var schoolsListViewModel: SchoolsListViewModel
    private val useCase: GetSchoolsListUseCase = mockk(relaxed = true)
    private val dispatcher = UnconfinedTestDispatcher()

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        schoolsListViewModel = SchoolsListViewModel(useCase, dispatcher)
    }

    @Test
    fun `get schools list success test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(
                UIResponse.Success(
                    listOf(
                        SchoolListUIData(
                            dbn = "02M260",
                            school_name = "Clinton School Writers & Artists, M.S. 260"
                        ),
                        SchoolListUIData(
                            dbn = "21K728",
                            school_name = "Liberation Diploma Plus High School"
                        )
                    )
                )
            )
        coEvery { useCase.getNYCSchoolsList() } returns mockFlow

        schoolsListViewModel.getSchoolList()

        Assert.assertTrue(schoolsListViewModel.schoolListLiveData.value?.size == 2)
        Assert.assertTrue(schoolsListViewModel.schoolListLiveData.value?.get(0)?.dbn == "02M260")
    }

    @Test
    fun `get schools empty list test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(UIResponse.Failure(DataResponseConstants.NO_RESULTS_FOUND))
        coEvery { useCase.getNYCSchoolsList() } returns mockFlow

        schoolsListViewModel.getSchoolList()

        Assert.assertTrue(schoolsListViewModel.errorMessageLiveData.value == DataResponseConstants.NO_RESULTS_FOUND)
    }

    @Test
    fun `get schools list failure test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(UIResponse.Failure(DataResponseConstants.ERROR_RESULTS))
        coEvery { useCase.getNYCSchoolsList() } returns mockFlow

        schoolsListViewModel.getSchoolList()

        Assert.assertTrue(schoolsListViewModel.errorMessageLiveData.value == DataResponseConstants.ERROR_RESULTS)
    }
}
