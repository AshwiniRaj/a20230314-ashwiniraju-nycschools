package com.example.a20230314_ashwiniraju_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SATScoresUIData
import com.example.a20230314_ashwiniraju_nycschools.usecase.GetSchoolSATScoreUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * A Unit test class for SchoolDetailViewModel.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
internal class SchoolDetailViewModelTest {
    private lateinit var schoolDetailViewModel: SchoolDetailViewModel
    private val useCase: GetSchoolSATScoreUseCase = mockk(relaxed = true)
    private val dispatcher = UnconfinedTestDispatcher()

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        schoolDetailViewModel = SchoolDetailViewModel(useCase, dispatcher)
    }

    @Test
    fun `get schools SAT scores success test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(
                UIResponse.Success(
                    SATScoresUIData(
                        dbn = "02M298",
                        schoolName = "PACE HIGH SCHOOL",
                        numOfSATTestTakers = "85",
                        satCriticalReadingAvgScore = "423",
                        satMathAvgScore = "438",
                        satWritingAvgScore = "432"
                    )
                )
            )
        coEvery { useCase.getSchoolSATScoreData(dbn = "02M298") } returns mockFlow

        schoolDetailViewModel.getSATScores(dbn = "02M298")

        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.schoolName == "PACE HIGH SCHOOL")
        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.dbn == "02M298")
        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.numOfSATTestTakers == "85")
        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.satCriticalReadingAvgScore == "423")
        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.satMathAvgScore == "438")
        Assert.assertTrue(schoolDetailViewModel.scoresLiveData.value?.satWritingAvgScore == "432")

    }

    @Test
    fun `get schools SAT scores empty data test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(UIResponse.Failure(DataResponseConstants.NO_RESULTS_FOUND))
        coEvery { useCase.getSchoolSATScoreData(dbn = "02M298") } returns mockFlow

        schoolDetailViewModel.getSATScores(dbn = "02M298")

        Assert.assertTrue(schoolDetailViewModel.errorMessageLiveData.value == DataResponseConstants.NO_RESULTS_FOUND)
    }

    @Test
    fun `get schools SAT scores failure test`() = runTest(dispatcher) {
        val mockFlow =
            MutableStateFlow(UIResponse.Failure(DataResponseConstants.ERROR_RESULTS))
        coEvery { useCase.getSchoolSATScoreData(dbn = "02M298") } returns mockFlow

        schoolDetailViewModel.getSATScores(dbn = "02M298")

        Assert.assertTrue(schoolDetailViewModel.errorMessageLiveData.value == DataResponseConstants.ERROR_RESULTS)
    }
}
