package com.example.a20230314_ashwiniraju_nycschools.repository

/**
 * sealed class which is used for generic network data response type.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
sealed class DataResponse {
    class Success<T>(val data: T) : DataResponse()
    class Failure(val message: String) : DataResponse()
}

/**
 * sealed class which is used for UI data response type.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
sealed class UIResponse {
    class Success<T>(val data: T) : UIResponse()
    class Failure(val message: String) : UIResponse()
}
