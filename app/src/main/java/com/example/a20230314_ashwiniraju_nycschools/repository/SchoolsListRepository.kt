package com.example.a20230314_ashwiniraju_nycschools.repository

import kotlinx.coroutines.flow.Flow

/**
 * Repository interface which used to make API calls.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
interface SchoolsListRepository {
    suspend fun getNYCSchoolsListData(): Flow<DataResponse>
    suspend fun getSATScoresData(dbn: String): Flow<DataResponse>
}
