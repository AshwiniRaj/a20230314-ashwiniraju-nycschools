package com.example.a20230314_ashwiniraju_nycschools.repository

import com.example.a20230314_ashwiniraju_nycschools.network.NYCSchoolsListAPI
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants.Companion.ERROR_RESULTS
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants.Companion.NO_RESULTS_FOUND
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Repository implementation class which used to make API calls and retrieve the data.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
class SchoolsListRepositoryImpl @Inject constructor(
    private val nycSchoolsListAPI: NYCSchoolsListAPI
) : SchoolsListRepository {

    /**
     * Method to retrieve the school list data by making network request.
     */
    override suspend fun getNYCSchoolsListData(): Flow<DataResponse> = flow {
        emit(
            try {
                val data = nycSchoolsListAPI.getNYCSchoolsList()
                if (data.isSuccessful) {
                    data.body()?.let {
                        if (it.isNotEmpty()) {
                            DataResponse.Success(it)
                        } else {
                            DataResponse.Failure(NO_RESULTS_FOUND)
                        }
                    } ?: run {
                        DataResponse.Failure(NO_RESULTS_FOUND)
                    }
                } else {
                    DataResponse.Failure(ERROR_RESULTS)
                }
            } catch (e: Exception) {
                DataResponse.Failure(ERROR_RESULTS)
            }
        )
    }

    /**
     * Method to retrieve the school SAT scores data by making network request.
     */
    override suspend fun getSATScoresData(dbn: String): Flow<DataResponse> = flow {
        emit(
            try {
                val data = nycSchoolsListAPI.getSATScoresData(dbn)
                if (data.isSuccessful) {
                    data.body()?.let {
                        if (it.isNotEmpty()) {
                            DataResponse.Success(it)
                        } else {
                            DataResponse.Failure(NO_RESULTS_FOUND)
                        }
                    } ?: run {
                        DataResponse.Failure(NO_RESULTS_FOUND)
                    }
                } else {
                    DataResponse.Failure(ERROR_RESULTS)
                }
            } catch (e: Exception) {
                DataResponse.Failure(ERROR_RESULTS)
            }
        )

    }
}
