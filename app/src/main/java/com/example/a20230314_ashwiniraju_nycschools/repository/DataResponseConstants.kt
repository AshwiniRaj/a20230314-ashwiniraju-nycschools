package com.example.a20230314_ashwiniraju_nycschools.repository

/**
 * Constant class which holds the network data constants.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
class DataResponseConstants {
    companion object {
        const val NO_RESULTS_FOUND = "No Results found."
        const val ERROR_RESULTS = "Error while fetching results."
    }
}
