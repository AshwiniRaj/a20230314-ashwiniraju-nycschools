package com.example.a20230314_ashwiniraju_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230314_ashwiniraju_nycschools.R
import com.example.a20230314_ashwiniraju_nycschools.adapter.SchoolListAdapter
import com.example.a20230314_ashwiniraju_nycschools.constants.DBN_KEY
import com.example.a20230314_ashwiniraju_nycschools.databinding.FragmentSchoolListBinding
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SchoolListUIData
import com.example.a20230314_ashwiniraju_nycschools.viewmodel.SchoolsListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass which is used to display the NYC school list information.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@AndroidEntryPoint
class SchoolListFragment : Fragment(), ItemClickListener {
    private lateinit var navController: NavController
    private lateinit var schoolsListViewModel: SchoolsListViewModel
    private lateinit var binding: FragmentSchoolListBinding
    private var schoolListAdapter: SchoolListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolsListViewModel = ViewModelProvider(this)[SchoolsListViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSchoolListBinding.inflate(
            LayoutInflater.from(requireContext()),
            container,
            false
        )

        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.school_list_title)
        schoolListAdapter = SchoolListAdapter(this)
        registerObservers()
        return binding.root
    }

    /**
     * Method to register the live data observers
     */
    private fun registerObservers() {
        schoolsListViewModel.schoolListLiveData.observe(viewLifecycleOwner) { schoolListUIData: List<SchoolListUIData?> ->
            schoolListAdapter?.submitList(schoolListUIData)
        }
        schoolsListViewModel.errorMessageLiveData.observe(viewLifecycleOwner) { s: String? ->
            schoolListAdapter?.submitList(emptyList())
            Toast.makeText(context, s, Toast.LENGTH_LONG).show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController(view)
        binding.schoolListRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = schoolListAdapter
        }
        schoolsListViewModel.schoolsList

    }

    override fun onItemClick(dbn: String?) {
        val bundle = Bundle()
        bundle.putString(DBN_KEY, dbn)
        navController.navigate(R.id.action_schoolListFragment_to_schoolDetailFragment, bundle)

    }
}