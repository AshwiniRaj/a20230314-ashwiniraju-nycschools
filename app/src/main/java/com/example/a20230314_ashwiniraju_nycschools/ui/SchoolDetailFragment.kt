package com.example.a20230314_ashwiniraju_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.a20230314_ashwiniraju_nycschools.R
import com.example.a20230314_ashwiniraju_nycschools.constants.DBN_KEY
import com.example.a20230314_ashwiniraju_nycschools.databinding.FragmentSchoolDetailBinding
import com.example.a20230314_ashwiniraju_nycschools.viewmodel.SchoolDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass which is used to display the school SAT scores and
 * other information.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@AndroidEntryPoint
class SchoolDetailFragment : Fragment() {
    private lateinit var mDBN: String
    private lateinit var schoolDetailViewModel: SchoolDetailViewModel
    private lateinit var binding: FragmentSchoolDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDBN = arguments?.getString(DBN_KEY) ?: ""
        schoolDetailViewModel = ViewModelProvider(this)[SchoolDetailViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSchoolDetailBinding.inflate(
            LayoutInflater.from(requireContext()),
            container,
            false
        )
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =
            getString(R.string.school_detail_title)

        binding.lifecycleOwner = this
        binding.viewModel = schoolDetailViewModel
        registerObservers()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        schoolDetailViewModel.getSATScores(mDBN)
    }

    /**
     * Method to register the live data observers
     */
    private fun registerObservers() {
        schoolDetailViewModel.errorMessageLiveData.observe(viewLifecycleOwner) { s: String? ->
            Toast.makeText(
                context, s, Toast.LENGTH_LONG
            ).show()
        }
    }
}