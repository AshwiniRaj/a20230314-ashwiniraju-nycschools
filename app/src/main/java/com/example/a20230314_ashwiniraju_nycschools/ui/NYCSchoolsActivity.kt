package com.example.a20230314_ashwiniraju_nycschools.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.a20230314_ashwiniraju_nycschools.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * NYCSchoolsActivity is a base activity for NYC Schools application
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@AndroidEntryPoint
class NYCSchoolsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nycschools)
    }
}
