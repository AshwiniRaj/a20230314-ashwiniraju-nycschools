package com.example.a20230314_ashwiniraju_nycschools.ui.model

/**
 * data class which holds the school list UI data.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
data class SchoolListUIData(
    val dbn: String,
    val school_name: String,
)
