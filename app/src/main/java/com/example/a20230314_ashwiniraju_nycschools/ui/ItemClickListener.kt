package com.example.a20230314_ashwiniraju_nycschools.ui

/**
 * Click Listener interface for recyclerview item click.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
interface ItemClickListener {
    /**
     * Method will get invoked when use clicks on school list item.
     *
     * @param dbn
     */
    fun onItemClick(dbn: String?)
}
