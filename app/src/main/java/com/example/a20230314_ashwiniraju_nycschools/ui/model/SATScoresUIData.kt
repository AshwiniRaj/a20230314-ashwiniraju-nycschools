package com.example.a20230314_ashwiniraju_nycschools.ui.model

/**
 * data class which holds the school SAT scores UI data.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
data class SATScoresUIData(
    val dbn: String,
    val schoolName: String,
    val numOfSATTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String,
)
