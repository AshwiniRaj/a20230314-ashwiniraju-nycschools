package com.example.a20230314_ashwiniraju_nycschools.hilt.di

import com.example.a20230314_ashwiniraju_nycschools.network.NYCSchoolsListAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Hilt Module for providing retrofit instances.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@InstallIn(SingletonComponent::class)
@Module
object NYCSchoolsListApiServiceModule {

    @Singleton
    @Provides
    fun provideNYCSchoolsListApiService(okHttpClient: OkHttpClient): NYCSchoolsListAPI {
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NYCSchoolsListAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder().addInterceptor(interceptor = HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }).build();
}
