package com.example.a20230314_ashwiniraju_nycschools.hilt.di

import com.example.a20230314_ashwiniraju_nycschools.repository.SchoolsListRepository
import com.example.a20230314_ashwiniraju_nycschools.repository.SchoolsListRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Hilt Module for providing repository instances.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@InstallIn(SingletonComponent::class)
@Module
abstract class NYCSchoolsRepoModule {

    @Binds
    abstract fun providesSchoolListRepository(schoolsListRepositoryImpl: SchoolsListRepositoryImpl): SchoolsListRepository

}

