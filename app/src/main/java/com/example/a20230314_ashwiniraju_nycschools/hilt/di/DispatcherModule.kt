package com.example.a20230314_ashwiniraju_nycschools.hilt.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Hilt Module for providing Coroutine Dispatcher instances.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@InstallIn(SingletonComponent::class)
@Module
object DispatcherModule {

    @Provides
    fun provideIODispatcher(): CoroutineDispatcher = Dispatchers.IO
}
