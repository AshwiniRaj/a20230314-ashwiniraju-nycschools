package com.example.a20230314_ashwiniraju_nycschools.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit Service interface which used to make API calls.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
interface NYCSchoolsListAPI {
    /**
     * API call to get NYC schools list data
     */
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getNYCSchoolsList(): Response<List<SchoolData>>

    /**
     * API call to get NYC schools SAT scores data
     * @param dbn - unique key for individual school
     */
    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSATScoresData(
        @Query("dbn") dbn: String
    ): Response<List<ScoresData>>
}
