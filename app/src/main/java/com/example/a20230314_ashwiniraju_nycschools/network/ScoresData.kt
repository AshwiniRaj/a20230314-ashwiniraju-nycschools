package com.example.a20230314_ashwiniraju_nycschools.network

/**
 * data class which holds the school SAT scores network data response.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
data class ScoresData(
    val dbn: String,
    val school_name: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String,
)
