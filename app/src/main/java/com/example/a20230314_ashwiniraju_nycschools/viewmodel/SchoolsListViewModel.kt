package com.example.a20230314_ashwiniraju_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants.Companion.ERROR_RESULTS
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SchoolListUIData
import com.example.a20230314_ashwiniraju_nycschools.usecase.GetSchoolsListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A ViewModel class for SchoolListFragment.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@HiltViewModel
class SchoolsListViewModel @Inject constructor(
    private val useCase: GetSchoolsListUseCase,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val schoolListMutableLiveData = MutableLiveData<List<SchoolListUIData>>()

    val schoolListLiveData = schoolListMutableLiveData as LiveData<List<SchoolListUIData>>

    private val errorMessageMutableLiveData = MutableLiveData<String>()
    val errorMessageLiveData = errorMessageMutableLiveData as LiveData<String>

    private val stateMutableLiveData = MutableLiveData<Boolean>()
    val stateLiveData: LiveData<Boolean> = stateMutableLiveData

    val schoolsList by lazy {
        getSchoolList()
    }

    /**
     * Method used to get school list data asynchronously.
     */
    fun getSchoolList() {
        viewModelScope.launch(ioDispatcher) {
            try {
                stateMutableLiveData.postValue(true)

                val result = useCase.getNYCSchoolsList()
                result.collect {
                    when (it) {
                        is UIResponse.Success<*> -> {
                            stateMutableLiveData.postValue(false)
                            schoolListMutableLiveData.postValue(
                                it.data as List<SchoolListUIData>
                            )

                        }
                        is UIResponse.Failure -> {
                            stateMutableLiveData.postValue(false)
                            errorMessageMutableLiveData.postValue(it.message)
                        }
                    }
                }
            } catch (e: Exception) {
                stateMutableLiveData.postValue(false)
                errorMessageMutableLiveData.postValue(ERROR_RESULTS)
            }
        }
    }
}
