package com.example.a20230314_ashwiniraju_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponseConstants
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SATScoresUIData
import com.example.a20230314_ashwiniraju_nycschools.usecase.GetSchoolSATScoreUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A ViewModel class for SchoolDetailFragment.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val useCase: GetSchoolSATScoreUseCase,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val scoresMutableLiveData = MutableLiveData<SATScoresUIData>()

    val scoresLiveData = scoresMutableLiveData as LiveData<SATScoresUIData>

    private val errorMessageMutableLiveData = MutableLiveData<String>()
    val errorMessageLiveData = errorMessageMutableLiveData as LiveData<String>

    private val stateMutableLiveData = MutableLiveData<Boolean>()
    val stateLiveData: LiveData<Boolean> = stateMutableLiveData

    /**
     * Method used to get school SAT scores data asynchronously.
     */
    fun getSATScores(dbn: String) {
        viewModelScope.launch(ioDispatcher) {
            try {
                stateMutableLiveData.postValue(true)
                val result = useCase.getSchoolSATScoreData(dbn)
                result.collect {
                    when (it) {
                        is UIResponse.Success<*> -> {
                            stateMutableLiveData.postValue(false)
                            scoresMutableLiveData.postValue(
                                it.data as SATScoresUIData
                            )
                        }
                        is UIResponse.Failure -> {
                            stateMutableLiveData.postValue(false)
                            errorMessageMutableLiveData.postValue(it.message)
                        }
                    }
                }
            } catch (e: Exception) {
                stateMutableLiveData.postValue(false)
                errorMessageMutableLiveData.postValue(DataResponseConstants.ERROR_RESULTS)
            }
        }
    }
}
