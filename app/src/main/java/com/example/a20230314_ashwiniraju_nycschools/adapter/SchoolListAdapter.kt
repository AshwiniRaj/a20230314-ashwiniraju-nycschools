package com.example.a20230314_ashwiniraju_nycschools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230314_ashwiniraju_nycschools.adapter.SchoolListAdapter.SchoolListViewHolder
import com.example.a20230314_ashwiniraju_nycschools.databinding.SchoolListItemBinding
import com.example.a20230314_ashwiniraju_nycschools.ui.ItemClickListener
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SchoolListUIData

/**
 * Adapter class to display School list data which extends ListAdapter.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
class SchoolListAdapter(private val clickListener: ItemClickListener) :
    ListAdapter<SchoolListUIData, SchoolListViewHolder>(
        DIFF_CALLBACK
    ) {

    private lateinit var binding: SchoolListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolListViewHolder {
        binding = SchoolListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return SchoolListViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: SchoolListViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class SchoolListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView.rootView) {
        fun bind(item: SchoolListUIData) {
            binding.schoolNameText = item.school_name
            itemView.setOnClickListener { view: View? ->
                clickListener.onItemClick(
                    getItem(adapterPosition).dbn
                )
            }
        }
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<SchoolListUIData> =
            object : DiffUtil.ItemCallback<SchoolListUIData>() {
                override fun areItemsTheSame(
                    oldData: SchoolListUIData, newData: SchoolListUIData
                ): Boolean {
                    return oldData === newData
                }

                override fun areContentsTheSame(
                    oldData: SchoolListUIData, newData: SchoolListUIData
                ): Boolean {
                    return oldData == newData
                }
            }
    }
}
