package com.example.a20230314_ashwiniraju_nycschools.constants

/**
 * A class to holds Constants Keys which is used for Bundle Objects.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
const val DBN_KEY = "dbn_key"
