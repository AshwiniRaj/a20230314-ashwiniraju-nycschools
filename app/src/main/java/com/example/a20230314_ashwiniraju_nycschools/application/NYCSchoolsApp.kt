package com.example.a20230314_ashwiniraju_nycschools.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * A Application class
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@HiltAndroidApp
class NYCSchoolsApp : Application()
