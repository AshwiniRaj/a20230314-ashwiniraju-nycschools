package com.example.a20230314_ashwiniraju_nycschools.usecase

import com.example.a20230314_ashwiniraju_nycschools.network.SchoolData
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponse
import com.example.a20230314_ashwiniraju_nycschools.repository.SchoolsListRepository
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SchoolListUIData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * A UseCase class which is used to get the NYC school list information.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
@Singleton
class GetSchoolsListUseCase @Inject constructor(
    private val schoolsListRepository: SchoolsListRepository
) {

    /**
     * Method to fetch the NYC school list
     */
    suspend fun getNYCSchoolsList(): Flow<UIResponse> = flow {
        val data = schoolsListRepository.getNYCSchoolsListData()
        data.collect {
            when (it) {
                is DataResponse.Success<*> -> {
                    val data = it.data as List<SchoolData>
                    val schoolUIList = mutableListOf<SchoolListUIData>()
                    data.map { schoolData ->
                        val schoolListUIData =
                            SchoolListUIData(
                                schoolData.dbn,
                                schoolData.school_name
                            )
                        schoolUIList.add(schoolListUIData)
                    }

                    emit(UIResponse.Success(schoolUIList))
                }
                is DataResponse.Failure -> {
                    emit(UIResponse.Failure(it.message))
                }
            }
        }

    }
}
