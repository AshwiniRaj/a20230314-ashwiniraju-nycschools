package com.example.a20230314_ashwiniraju_nycschools.usecase

import com.example.a20230314_ashwiniraju_nycschools.network.ScoresData
import com.example.a20230314_ashwiniraju_nycschools.repository.DataResponse
import com.example.a20230314_ashwiniraju_nycschools.repository.SchoolsListRepository
import com.example.a20230314_ashwiniraju_nycschools.repository.UIResponse
import com.example.a20230314_ashwiniraju_nycschools.ui.model.SATScoresUIData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * A UseCase class which is used to get the NYC school SAT scores information.
 * Created by {@Author Ashwini Raju} on {@Date 03/14/2023}
 */
class GetSchoolSATScoreUseCase @Inject constructor(
    private val schoolsListRepository: SchoolsListRepository
) {

    /**
     * Method to fetch the NYC school SAT scores
     */
    suspend fun getSchoolSATScoreData(dbn: String): Flow<UIResponse> =
        flow {
            schoolsListRepository.getSATScoresData(dbn).collect {
                when (it) {
                    is DataResponse.Success<*> -> {
                        val data = it.data as List<ScoresData>
                        val scoresData = SATScoresUIData(
                            dbn = data[0].dbn,
                            schoolName = data[0].school_name,
                            numOfSATTestTakers = data[0].num_of_sat_test_takers,
                            satCriticalReadingAvgScore = data[0].sat_critical_reading_avg_score,
                            satMathAvgScore = data[0].sat_math_avg_score,
                            satWritingAvgScore = data[0].sat_writing_avg_score,
                        )
                        emit(UIResponse.Success(scoresData))
                    }
                    is DataResponse.Failure -> {
                        emit(UIResponse.Failure(it.message))
                    }
                }
            }
        }


}